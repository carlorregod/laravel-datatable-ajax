<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Modelo
use App\Agenda;
use Datatables;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $datos = Agenda::latest()->get();
            return datatables()->of($datos)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<button data-id="'.$row->id.'" data-original-title="Editar" class="edit btn btn-primary btn-sm editAgenda">Editar</button>
                           <button data-id="'.$row->id.'" data-original-title="Borrar" class="btn btn-danger btn-sm deleteAgenda">Borrar</button>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true); 
        }
        return view('/',compact('agendas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            //Buscar_correo
            $busca_correo = Agenda::where('email',$request->correo)->first();
            if($busca_correo){
                $mensaje = "Usuario con correo en uso";
            }
            else{
                $nuevo = new Agenda();
                $nuevo->nombre = $request->nombre;
                $nuevo->edad = $request->edad;
                $nuevo->telefono = $request->fono;
                $nuevo->email = $request->correo; //Correo deberá ser único
                $nuevo->save(); //Para guardar el registro
                $mensaje = "Registro guardado exitosamente";
            }
            return $mensaje;
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        if($request->ajax())
        {
            $data = Agenda::find($id);
            return response()->json($data);
        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try 
        {
            $reg_a_actualizar = Agenda::find($id); //Encontrar los registros con el $id pasado
            $reg_a_actualizar->nombre = $request->nombre;
            $reg_a_actualizar->edad = $request->edad;
            $reg_a_actualizar->telefono = $request->telefono;
            $reg_a_actualizar->email = $request->email;
            $reg_a_actualizar->save();//Actualización completada
            return "Registro actualizado exitosamente";
        }
        catch(Exception $e)
        {
            return "Problemas con la actualización. Correo hallado en otro registro";
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        if($request->ajax())
        {
            try
            {
                $eliminar = Agenda::destroy($id); //también se podría usar el método Agenda::find($id)->delete()
                return "Elemento eliminado exitosamente";
            }
            catch(Exception $e)
            {
                return "Hubo un error ".$e->getMessage();
            }
        }
    }


 
}
