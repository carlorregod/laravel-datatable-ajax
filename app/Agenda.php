<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
	protected $table="agendas";
    protected $fillable = ['id', 'nombre', 'edad', 'telefono', 'email'];
    protected $primaryKey = 'id';
}
