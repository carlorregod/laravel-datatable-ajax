<?php

use Illuminate\Database\Seeder;
use App\Agenda;

class seedAgenda extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Agenda::class,50)->create();
    }
}
