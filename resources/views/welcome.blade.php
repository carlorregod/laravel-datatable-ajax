<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <style type="text/css">
            .dataTables{
                font-size: 10px;
            }
        </style>
        <title>CRUD con Laravel y Datatables</title>
    </head>
    <body>
      {{-- Alerta standard --}}
      <div class="alert alert-primary alert-dismissible fade show" role="alert" id="mostrar-alerta">
        <div id=mensaje-alerta></div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <!-- Cuerpo del modal formulario nuevo registro -->
        <div class="modal fade bd-example-modal-lg" id="nuevoRegistroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nuevo Ingreso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <!--form -->
                  <div class="form-group">
                    <label for="nombre" class="col-form-label">Nombre:</label>
                    <input type="text" class="form-control" id="nombre">
                  </div>
                  <div class="form-group">
                    <label for="edad" class="col-form-label">Edad:</label>
                    <input type="number" min="1" max="99" class="form-control" id="edad"></input>
                  </div>
                  <div class="form-group">
                    <label for="fono" class="col-form-label">Teléfono:</label>
                    <input type="text" class="form-control" id="fono"></input>
                  </div>
                  <div class="form-group">
                    <label for="correo" class="col-form-label">Correo:</label>
                    <input type="email" class="form-control" id="correo"></input>
                  </div>
                <!-- /form -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btn-nuevo">Nuevo registro</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Cuerpo del modal formulario actualizar registro -->
        <div class="modal fade bd-update-modal-lg" id="actualizarRegistroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Actualizar Registro</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div id="opcion_a_actualizar" hidden></div> {{-- Importante para la actualización! --}}
                  <!--form -->
                    <div class="form-group">
                      <label for="nombre" class="col-form-label">Nombre:</label>
                      <input type="text" class="form-control" id="nombre_edit">
                    </div>
                    <div class="form-group">
                      <label for="edad" class="col-form-label">Edad:</label>
                      <input type="number" min="1" max="99" class="form-control" id="edad_edit"></input>
                    </div>
                    <div class="form-group">
                      <label for="fono" class="col-form-label">Teléfono:</label>
                      <input type="text" class="form-control" id="fono_edit"></input>
                    </div>
                    <div class="form-group">
                      <label for="correo" class="col-form-label">Correo:</label>
                    <input type="email" class="form-control" id="correo_edit"></input>
                    </div>
                  <!-- /form -->
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-primary" id="btn-actualizar">Actualizar Registro</button>
                </div>
              </div>
            </div>
          </div>
        {{-- Cuerpo del modal de eliminacion de registro --}}
        <div class="modal modal-eliminando" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Eliminar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>¿Seguro desea eliminar el registro? Esto no puede ser revertido</p>
                <div id="opcion_a_eliminar" hidden></div> {{-- Importante para futura eliminación! --}}
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="eliminar_registro">Eliminar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              </div>
            </div>
          </div>
        </div>
        <!--Título -->
        <hr>
        <h1>CRUD con Laravel y Datatables server side </h1>
        <hr>
        {{-- Botón para nuevo registro --}}
        <button id="nuevo-registro" type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Nuevo...</button>
        <hr><br>
        <!--Tablita -->
        <div class="container">
            <table id="tabla">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Edad</th>
                        <th>Telefono</th>
                        <th>Correo</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <script type="text/javascript">
            var idioma_espaniol = {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            };

            var tabla_de_registros = $('#tabla').DataTable({
                    processing: true,
                    serverSide: true,

                    ajax: {
                        url: 'ajax',
                        dataSrc: 'data'
                    },
                    language: idioma_espaniol,
                    columns: [ 
                        { "data": "nombre", "name": "nombre" },
                        { "data": "edad", "name": "edad" },
                        { "data": "telefono", "name":"telefono" },
                        { "data": "email","name":"email" },
                        { "data": 'action', "name": 'action', orderable: false},
                     ]
                });
            $(document).ready(function (){
                //Alerta oculta
                $('#mostrar-alerta').hide();
                //Preparando el ajax..
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                //Llamada a la tabla
                tabla_de_registros;

            });
            //Nuevo registro..
            $('#btn-nuevo').click(function(e){
                e.preventDefault;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var datos = "&nombre="  +$('#nombre').val()+
                            "&edad="    +$('#edad').val()+
                            "&fono="    +$('#fono').val()+
                            "&correo="  +$('#correo').val();
                var url = "ajax";
                $.ajax({
                    url: url,
                    data: datos,
                    type: "POST",

                })
                .done(function(mensaje){
                    $('#mensaje-alerta').html(mensaje); //Muestra mensake
                    $('#mostrar-alerta').show();        //Muestra el alert
                    $('#nuevoRegistroModal').modal('hide');//Oculta el modal
                    tabla_de_registros.draw(); //Actualización de tabla
                })
                .fail(function(error){
                    console.log("error.."+error.getMessage);
                }); 
            });
            //Actualizando... mostrando modal
            $(document).on('click','.editAgenda',function(e){
              e.preventDefault;
              $('.bd-update-modal-lg').modal('show');
              $('#opcion_a_actualizar').html($(this).attr('data-id'));
              //Llamando los valores por defecto en el formulario
              var id=$(this).attr('data-id');
              $.ajax({
                url: 'ajax/'+id,
                type: 'GET'
              })
              .done(function(data){
                console.log('lista la carga...');
                $('#nombre_edit').val(data.nombre);
                $('#edad_edit').val(data.edad);
                $('#fono_edit').val(data.telefono);
                $('#correo_edit').val(data.email);
              })
              .fail(function(error){
                    console.log("error.."+error.getMessage);
                }); 
            });
            //Actualizando, la ejecución
            $('#btn-actualizar').click(function(e){
              e.preventDefault;
              $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
              $.ajax({
                'url': 'ajax/'+$('#opcion_a_actualizar').html(),
                'type':'PUT',
                'data': {
                  'nombre':   $('#nombre_edit').val(),
                  'edad':     $('#edad_edit').val(),
                  'telefono': $('#fono_edit').val(),
                  'email':    $('#correo_edit').val()
                }
              })
              .done(function(mensaje){
                    $('#mensaje-alerta').html(mensaje); //Muestra mensake
                    $('#mostrar-alerta').show();        //Muestra el alert
                    $('#actualizarRegistroModal').modal('hide');//Oculta el modal
                    tabla_de_registros.draw(); //Actualización de tabla
                })
              .fail(function(error){
                    console.log("error.."+error.getMessage);
                }); 
            });
            

            //Eliminando...
            $(document).on('click','.deleteAgenda',function(e){
              e.preventDefault;
              $('.modal-eliminando').modal('show');
              $('#opcion_a_eliminar').html($(this).attr('data-id'));
            });
            $('#eliminar_registro').click(function(){
              var opcion_eliminar = $('#opcion_a_eliminar').html();
              var url = 'ajax/'+opcion_eliminar;
              $.ajax({
                'url': url,
                'type': 'DELETE'
              })
              .done(function(mensaje){
                $('#mensaje-alerta').html(mensaje); //Muestra mensake
                $('#mostrar-alerta').show();        //Muestra el alert
                $('.modal-eliminando').modal('hide');//Oculta el modal
                tabla_de_registros.draw(); //Actualización de tabla
              })
              .fail(function(error){
                console.log("error.."+error.getMessage);
              })
            });
        </script>
    </body>
</html>
